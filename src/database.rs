use std::error::Error;
use std::fmt::{Display, Formatter};
use rusqlite::params;
use serenity::prelude::TypeMapKey;
use tokio::sync::mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender};
use tokio::sync::oneshot;
use tracing::info;
use crate::database::AnniversaryType::{Birthday, Relationship};
use crate::database::AnniversaryTypeError::{InvalidInteger, InvalidString};

#[derive(Debug, Clone, Copy)]
pub enum AnniversaryType {
    Birthday,
    Relationship,
}

#[derive(Debug)]
pub enum AnniversaryTypeError {
    InvalidInteger(u8),
    InvalidString(String)
}

impl Display for AnniversaryTypeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Invalid anniversary type: {:?}", self)
    }
}

impl Error for AnniversaryTypeError {}

impl TryFrom<&str> for AnniversaryType {
    type Error = AnniversaryTypeError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let lower = value.to_ascii_lowercase();
        match lower.as_str() {
            "birthday" => Ok(Birthday),
            "relationship" => Ok(Relationship),
            _ => Err(InvalidString(lower))
        }
    }
}

impl TryFrom<u8> for AnniversaryType {
    type Error = AnniversaryTypeError;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Birthday),
            1 => Ok(Relationship),
            _ => Err(InvalidInteger(value))
        }
    }
}

#[derive(Debug)]
pub struct Anniversary {
    pub user: u64,
    pub guild: u64,
    pub member: String,
    pub a_type: AnniversaryType,
    pub first_year: Option<u16>,
    pub month: u8,
    pub day: u8,
}

#[derive(Debug)]
pub struct PartialAnniversary {
    pub first_year: Option<u16>,
    pub a_type: AnniversaryType,
    pub member: String,
    pub user: u64,
    pub guild: u64,
    pub channel: u64,
}

#[derive(Debug)]
enum Task {
    // syntax: TaskName(oneshot::Sender<ReturnType>, /* param_name */ ParamType ...),
    AddMember(
        oneshot::Sender<rusqlite::Result<usize>>,
        /* name */ String,
        /* user id */ u64,
        /* guild */ u64,
        /* image */ Option<String>,
    ),
    DelMember(
        oneshot::Sender<rusqlite::Result<usize>>,
        /* name */ String,
        /* user id */ u64,
        /* guild */ u64,
    ),
    AddAnniversary(
        oneshot::Sender<rusqlite::Result<usize>>,
        /* anniversary */ Anniversary,
    ),
    GetAnniversariesToday(
        oneshot::Sender<rusqlite::Result<Vec<PartialAnniversary>>>
    ),
    WipeUser(
        oneshot::Sender<rusqlite::Result<usize>>,
        /* id */ u64,
        /* guild */ u64,
    ),
    RegisterGuild(
        oneshot::Sender<rusqlite::Result<usize>>,
        /* guild */ u64,
        /* channel */ u64,
    ),
    UnregisterGuild(
        oneshot::Sender<rusqlite::Result<usize>>,
        /* guild */ u64,
    ),
    IsGuildRegistered(
        oneshot::Sender<rusqlite::Result<bool>>,
        /* guild */ u64,
    )
}

pub struct DbExecutor {
    rx: UnboundedReceiver<Task>,
    db: rusqlite::Connection,
}

impl DbExecutor {
    pub fn create(dbpath: &str) -> rusqlite::Result<(Self, ExecutorConnection)> {
        let (tx, rx) = unbounded_channel();
        let db = rusqlite::Connection::open(dbpath)?;
        db.execute("pragma foreign_keys = on", [])?;
        db.execute(
        "create table if not exists guilds(\
            id integer primary key unique,\
            announce_channel integer not null\
        ) without rowid;", [])?;
        db.execute(
        "create table if not exists members(\
            id integer primary key,\
            name text not null,\
            user integer not null,\
            guild integer not null,\
            image text,\
            foreign key(guild) references guilds(id) on delete cascade,\
            unique(user, guild, name)\
        );", [])?;
        db.execute(
        "create table if not exists anniversaries(\
            id integer primary key,\
            member integer not null,\
            type integer not null,\
            last_announce integer not null default 0,\
            first_year integer,\
            month integer not null,\
            day integer not null,\
            foreign key(member) references members(id) on delete cascade,\
            unique(member, type)\
        );", [])?;
        info!("Database executor for '{}' opened!", dbpath);
        Ok((Self { rx, db }, ExecutorConnection(tx)))
    }

    pub fn run(&mut self) {
        while let Some(task) = self.rx.blocking_recv() {
            match task {
                Task::AddMember(tx, n, id, g, i) => {
                    tx.send(self.db.execute(
                        "insert or ignore into members(name, user, guild, image) values(?,?,?,?)",
                        params![n, id, g, i]
                    )).unwrap();
                },
                Task::DelMember(tx, n, id, g) => {
                    tx.send(self.db.execute(
                        "delete from members where user=? and name=? and guild=?",
                        params![id, n, g]
                    )).unwrap();
                },
                Task::AddAnniversary(tx, a) => {
                    tx.send(self.db.execute(
                        "replace into anniversaries(member, type, first_year, month, day) select id,?,?,?,? from members where user=? and name=? and guild=?",
                        params![a.a_type as u8, a.first_year, a.month, a.day, a.user, a.member, a.guild]
                    )).unwrap();
                },
                Task::WipeUser(tx, id, g) => {
                    tx.send(self.db.execute("delete from members where user=? and guild=?", [id, g])).unwrap();
                },
                Task::RegisterGuild(tx, g, c) => {
                    tx.send(self.db.execute(
                        "insert into guilds(id, announce_channel) values(?1,?2) on conflict(id) do update set announce_channel=?2",
                        [g, c]
                    )).unwrap();
                }
                Task::UnregisterGuild(tx, g) => {
                    tx.send(self.db.execute("delete from guilds where id=?", [g])).unwrap();
                }
                Task::IsGuildRegistered(tx, g) => {
                    tx.send(self.db.query_row(
                        "select exists(select 1 from guilds where id=?)",
                        [g], |v| v.get::<_,bool>(0))
                    ).unwrap();
                }
                Task::GetAnniversariesToday(tx) => {
                    tx.send(self.get_anniversaries_helper()).unwrap();
                }
            }
        }
    }

    fn get_anniversaries_helper(&self) -> rusqlite::Result<Vec<PartialAnniversary>> {
        let mut stmt = self.db.prepare(
            "select m.user,m.name,g.announce_channel,a.type,a.first_year,a.id,m.guild \
            from anniversaries as a \
            inner join members as m on m.id=a.member \
            inner join guilds as g on g.id=m.guild \
            where a.last_announce != cast(strftime('%Y',date('now')) as integer) \
            and a.month = cast(strftime('%m',date('now')) as integer) \
            and a.day = cast(strftime('%d',date('now')) as integer)"
        )?;
        let mut rows = stmt.query([])?;
        let mut anniversaries = Vec::new();
        let mut incr_stmt = self.db.prepare(
            "update anniversaries set last_announce = strftime('%Y',date('now')) where id=?"
        )?;
        while let Some(r) = rows.next()? {
            incr_stmt.execute([r.get::<_,u64>(5)?])?;
            anniversaries.push(PartialAnniversary {
                user: r.get(0)?,
                member: r.get(1)?,
                channel: r.get(2)?,
                a_type: AnniversaryType::try_from(r.get::<_,u8>(3)?).unwrap(),
                first_year: r.get(4)?,
                guild: r.get(6)?,
            });
        }
        Ok(anniversaries)
    }
}

pub struct ExecutorConnection(UnboundedSender<Task>);

impl Clone for ExecutorConnection {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl TypeMapKey for ExecutorConnection {
    type Value = Self;
}

macro_rules! executor_wrapper {
    ($name:ident, $task:expr, $ret:ty, $($arg:ident: $ty:ty),*) => {
        pub async fn $name(&self, $($arg: $ty),*) -> $ret {
          let (otx, orx) = oneshot::channel();
          self.0.send($task(otx, $($arg),*)).unwrap();
          orx.await.unwrap()
        }
    };
    ($name:ident, $task:expr, $ret:ty) => {
        pub async fn $name(&self) -> $ret {
          let (otx, orx) = oneshot::channel();
          self.0.send($task(otx)).unwrap();
          orx.await.unwrap()
        }
    };
}

impl ExecutorConnection {
    executor_wrapper!(
        add_member, Task::AddMember, rusqlite::Result<usize>,
        name: String, user: u64, guild: u64, image: Option<String>
    );
    executor_wrapper!(
        delete_member, Task::DelMember, rusqlite::Result<usize>,
        name: String, user: u64, guild: u64
    );
    executor_wrapper!(
        add_anniversary, Task::AddAnniversary, rusqlite::Result<usize>,
        anniversary: Anniversary
    );
    executor_wrapper!(
        register_guild, Task::RegisterGuild, rusqlite::Result<usize>,
        guild: u64, channel: u64
    );
    executor_wrapper!(
        unregister_guild, Task::UnregisterGuild, rusqlite::Result<usize>,
        guild: u64
    );
    executor_wrapper!(
        is_guild_registered, Task::IsGuildRegistered, rusqlite::Result<bool>,
        guild: u64
    );
    executor_wrapper!(
        wipe_user, Task::WipeUser, rusqlite::Result<usize>,
        user: u64, guild: u64
    );
    executor_wrapper!(
        get_anniversaries_today, Task::GetAnniversariesToday, rusqlite::Result<Vec<PartialAnniversary>>,
    );
}
