use std::error::Error;
use std::fmt::{Display, Formatter};
use anyhow::anyhow;
use chrono::NaiveDate;
use ordinal::Ordinal;
use serenity::builder::{CreateApplicationCommands, CreateEmbed, CreateInteractionResponseData};
use serenity::client::Context;
use serenity::model::interactions::application_command::{ApplicationCommandInteraction, ApplicationCommandOptionType};
use serenity::model::prelude::application_command::ApplicationCommandInteractionDataOptionValue;
use crate::database::{Anniversary, AnniversaryType};
use crate::ExecutorConnection;
use num_traits::cast::FromPrimitive;
use serenity::model::channel::ChannelType;

pub fn register_commands(commands: &mut CreateApplicationCommands) -> &mut CreateApplicationCommands {
    commands
        .create_application_command(|command| {
            command
                .name("addmember")
                .description("Add a family member")
                .create_option(|option| {
                    option
                        .name("name")
                        .description("Name of the family member")
                        .kind(ApplicationCommandOptionType::String)
                        .required(true)
                })
                .create_option(|option| {
                    option
                        .name("image")
                        .description("*Discord* image URL associated with the family member")
                        .kind(ApplicationCommandOptionType::String)
                })
        })
        .create_application_command(|command| {
            command
                .name("delmember")
                .description("Remove a family member")
                .create_option(|option| {
                    option
                        .name("name")
                        .description("Name of the family member")
                        .kind(ApplicationCommandOptionType::String)
                        .required(true)
                })
        })
        .create_application_command(|command| {
            command
                .name("addanniversary")
                .description("Set an anniversary for a family member")
                .create_option(|option| {
                    option
                        .name("member")
                        .description("Family member name")
                        .kind(ApplicationCommandOptionType::String)
                        .required(true)
                })
                .create_option(|option| {
                    option
                        .name("type")
                        .description("Anniversary type - either 'birthday' or 'relationship'")
                        .kind(ApplicationCommandOptionType::String)
                        .required(true)
                })
                .create_option(|option| {
                    option
                        .name("month")
                        .description("Anniversary month")
                        .kind(ApplicationCommandOptionType::Integer)
                        .required(true)
                })
                .create_option(|option| {
                    option
                        .name("day")
                        .description("Anniversary day")
                        .kind(ApplicationCommandOptionType::Integer)
                        .required(true)
                })
                .create_option(|option| {
                    option
                        .name("year")
                        .description("Beginning year (e.g. birth year for birthdays)")
                        .kind(ApplicationCommandOptionType::Integer)
                })
        })
        .create_application_command(|command| {
            command
                .name("register")
                .description("Register the current guild (requires admin permissions)")
                .create_option(|option| {
                    option
                        .name("channel")
                        .description("Channel for anniversary annoucements")
                        .kind(ApplicationCommandOptionType::Channel)
                        .required(true)
                })
        })
        .create_application_command(|command| {
            command
                .name("unregister")
                .description("Unregister the current guild (WARNING: this will wipe all associated user data)")
        })
        .create_application_command(|command| {
            command
                .name("guide")
                .description("Display bot guide")
        })
}

pub enum SlashCommandResult {
    Text(String),
    Embed(CreateEmbed)
}

impl SlashCommandResult {
    pub fn apply(self, v: &mut CreateInteractionResponseData) -> &mut CreateInteractionResponseData {
        match self {
            SlashCommandResult::Text(o) => v.content(o),
            SlashCommandResult::Embed(o) => v.add_embed(o)
        }
    }
}

#[derive(Debug, Default)]
pub struct InvalidCommandError;

impl Display for InvalidCommandError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("Invalid command or arguments")
    }
}

impl Error for InvalidCommandError {}

pub async fn handle_slash_command(cmd: &ApplicationCommandInteraction, ctx: &Context) -> anyhow::Result<SlashCommandResult> {
    let options = &cmd.data.options;
    let db = ctx.data.read().await.get::<ExecutorConnection>().unwrap().clone();
    let guild_id = cmd.guild_id.ok_or_else(InvalidCommandError::default)?.0;
    match cmd.data.name.as_str() {
        "addmember" => {
            if !db.is_guild_registered(guild_id).await? {
                return Ok(SlashCommandResult::Text("**Error:** This guild is not registered, see /guide for more information.".into()))
            }
            let name = options.get(0)
                .and_then(|v| v.value.as_ref())
                .and_then(|v| v.as_str())
                .ok_or_else(InvalidCommandError::default)?;
            let img = options.get(1)
                .and_then(|v| v.value.as_ref())
                .and_then(|v| v.as_str())
                .map(ToString::to_string);
            if let Some(img) = &img {
                if !img.starts_with("https://cdn.discordapp.com/attachments/") && !img.starts_with("https://media.discordapp.net/attachments/") {
                    return Ok(SlashCommandResult::Text("**Error:** The supplied image URL doesn't seem to be from Discord.".into()))
                }
            }
            db.add_member(
                name.to_string(),
                cmd.user.id.0,
                guild_id,
                img
            ).await?;
            Ok(SlashCommandResult::Text(format!("Family member *\"{}\"* has been added.", name)))
        }
        "delmember" => {
            let name = options.get(0)
                .and_then(|v| v.value.as_ref())
                .and_then(|v| v.as_str())
                .ok_or_else(InvalidCommandError::default)?;
            if db.delete_member(name.into(), cmd.user.id.0, guild_id).await? == 0 {
                Ok(SlashCommandResult::Text(format!("**Error:** Family member *\"{}\"* not found in this guild.", name)))
            } else {
                Ok(SlashCommandResult::Text(format!("Family member *\"{}\"* has been removed.", name)))
            }
        }
        "addanniversary" => {
            let month = options.get(2)
                .and_then(|v| v.value.as_ref())
                .and_then(|v| v.as_u64())
                .and_then(|v| u8::from_u64(v))
                .ok_or_else(InvalidCommandError::default)?;
            let day = options.get(3)
                .and_then(|v| v.value.as_ref())
                .and_then(|v| v.as_u64())
                .and_then(|v| u8::from_u64(v))
                .ok_or_else(InvalidCommandError::default)?;
            if !(1..=12).contains(&month) || !(1..=31).contains(&day) {
                return Ok(SlashCommandResult::Text("**Error:** Invalid month or day specified.".into()))
            }

            let member = options.get(0)
                .and_then(|v| v.value.as_ref())
                .and_then(|v| v.as_str())
                .ok_or_else(InvalidCommandError::default)?;
            let a_type = options.get(1)
                .and_then(|v| v.value.as_ref())
                .and_then(|v| v.as_str())
                .ok_or_else(InvalidCommandError::default)?;
            let first_year = options.get(4)
                .and_then(|v| v.value.as_ref())
                .and_then(|v| v.as_u64())
                .and_then(|v| u16::from_u64(v));
            let a_type = match AnniversaryType::try_from(a_type) {
                Err(e) => {
                    return Ok(SlashCommandResult::Text(format!("**Error:** {}", e)))
                }
                Ok(o) => o
            };
            let result = db.add_anniversary(Anniversary {
                user: cmd.user.id.0,
                guild: guild_id,
                member: member.into(),
                a_type,
                first_year,
                month,
                day
            }).await?;
            if result == 0 {
                Ok(SlashCommandResult::Text(format!("**Error:** Family member *\"{}\"* not found in this guild.", member)))
            } else {
                let month = NaiveDate::from_ymd(1, month as u32, 1).format("%B");
                Ok(SlashCommandResult::Text(
                    format!("**{:?}** anniversary set for family member *\"{}\"* on {} **{}**{}.",
                            a_type, member, month, day, Ordinal(day).suffix())
                ))
            }
        }
        "register" => {
            let permissions = cmd.member.as_ref()
                .ok_or_else(InvalidCommandError::default)?
                .permissions
                .ok_or_else(InvalidCommandError::default)?;
            if !permissions.administrator() {
                return Ok(SlashCommandResult::Text("**Error:** Insufficient permissions.".into()))
            }

            let channel = cmd.data.options
                .get(0)
                .ok_or_else(InvalidCommandError::default)?
                .resolved.as_ref()
                .ok_or_else(InvalidCommandError::default)?;
            let channel = if let ApplicationCommandInteractionDataOptionValue::Channel(c) = channel {
                c
            } else {
                return Err(anyhow!(InvalidCommandError::default()))
            };

            if channel.kind != ChannelType::Text {
                return Ok(SlashCommandResult::Text("**Error:** `channel` must be a text channel.".into()))
            }

            db.register_guild(guild_id, channel.id.0).await?;
            Ok(SlashCommandResult::Text(
                format!("Guild has been registered successfully \
                with channel <#{}> for the anniversary announcements.", channel.id.0)
            ))
        }
        "unregister" => {
            let permissions = cmd.member.as_ref()
                .ok_or_else(InvalidCommandError::default)?
                .permissions
                .ok_or_else(InvalidCommandError::default)?;
            if !permissions.administrator() {
                return Ok(SlashCommandResult::Text("**Error:** Insufficient permissions.".into()))
            }
            if !db.is_guild_registered(guild_id).await? {
                return Ok(SlashCommandResult::Text("**Error:** This guild is not registered.".into()))
            }
            db.unregister_guild(guild_id).await?;
            Ok(SlashCommandResult::Text("Guild has been unregistered successfully.".into()))
        }
        "guide" => {
            if let Err(_) = cmd.user.dm(&ctx, |m| m.content("there is no guide.")).await {
                Ok(SlashCommandResult::Text("I tried to DM you the guide, but failed in doing so. \
                                             Make sure that your DMs from this server are open.".into()))
            } else {
                Ok(SlashCommandResult::Text("DMed you the guide!".into()))
            }
        }
        _ => Err(anyhow!(InvalidCommandError::default()))
    }
}