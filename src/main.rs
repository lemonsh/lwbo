use std::{env, thread};
use std::sync::Arc;
use anyhow::anyhow;
use chrono::{Datelike, NaiveTime, Timelike, Utc};
use ordinal::Ordinal;

use serenity::Client;
use serenity::client::{Context, EventHandler};
use serenity::model::gateway::Ready;
use serenity::model::interactions::{Interaction, InteractionResponseType};
use serenity::model::interactions::application_command::ApplicationCommand;
use serenity::async_trait;
use serenity::http::Http;
use serenity::model::id::{ChannelId, UserId};
use tokio::sync::oneshot;
use tokio::time::sleep;
use tracing::{debug, error, info, warn};
use tracing_subscriber::EnvFilter;

use crate::database::{DbExecutor, ExecutorConnection};

mod database;
mod commands;

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("Logged in as {}#{}", ready.user.name, ready.user.discriminator);
        ApplicationCommand::set_global_application_commands(&ctx.http, commands::register_commands).await.unwrap();
        info!("Registered slash commands");
    }

    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        if let Interaction::ApplicationCommand(command) = interaction {
            match commands::handle_slash_command(&command, &ctx).await {
                Ok(result) => {
                    let reply = command
                        .create_interaction_response(&ctx.http, |response| {
                            response
                                .kind(InteractionResponseType::ChannelMessageWithSource)
                                .interaction_response_data(|v| result.apply(v))
                        }).await;

                    if let Err(e) = reply {
                        warn!("Couldn't respond to slash command: {}", e);
                    }
                }
                Err(e) => {
                    let _res = command
                        .create_interaction_response(&ctx.http, |response| {
                            response
                                .kind(InteractionResponseType::ChannelMessageWithSource)
                                .interaction_response_data(|v| v.content(format!("Internal error: `{}`", e)))
                        }).await;
                    warn!("Error in slash command handler: {}", e);
                }
            }
        }
    }
}

async fn terminate_signal() {
    use tokio::signal::unix::{signal, SignalKind};
    let mut sigterm = signal(SignalKind::terminate()).unwrap();
    let mut sigint = signal(SignalKind::interrupt()).unwrap();
    debug!("Installed ctrl+c handler");
    tokio::select! {
        _ = sigterm.recv() => {},
        _ = sigint.recv() => {}
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_env("LWBO_LOG"))
        .init();

    let token = env::var("LWBO_TOKEN")?;
    let application_id: u64 = env::var("LWBO_APPID")?.parse()?;
    let dbpath = env::var("LWBO_DB");
    let dbpath = dbpath.as_deref().unwrap_or("lwbo.db");

    let time: String = env::var("LWBO_DCTIME")?;
    let time = time.splitn(2, ':').collect::<Vec<_>>();
    let time = NaiveTime::from_hms(
        time.get(0).ok_or_else(|| anyhow!("Invalid time format"))?.parse()?,
        time.get(1).ok_or_else(|| anyhow!("Invalid time format"))?.parse()?,
        0
    );
    info!("Daily check time set to {}", time);

    let (mut db_exec, db_conn) = DbExecutor::create(dbpath)?;
    let db_thread = thread::spawn(move || {
        db_exec.run();
        info!("Database executor finished")
    });

    let mut client = Client::builder(token)
        .event_handler(Handler)
        .application_id(application_id)
        .await?;
    let shard_manager = client.shard_manager.clone();
    let http = client.cache_and_http.http.clone();

    {
        let mut data = client.data.write().await;
        data.insert::<ExecutorConnection>(db_conn.clone());
    }

    let client_task = tokio::spawn(async move {
        if let Err(e) = client.start().await {
            error!("Error while running the Discord client: {}", e);
        }
        info!("Discord client has been shut down successfully");
    });

    let daily_token = oneshot::channel::<()>();
    let selfid = http.get_current_user().await?.id;
    let daily_task = tokio::spawn(daily_check(http, db_conn, time, selfid, daily_token.1));

    terminate_signal().await;
    info!("Shutdown signal received, powering down");

    let _err = daily_token.0.send(());
    daily_task.await.unwrap_or_else(|e| error!("Failed to join daily task: {}", e));

    shard_manager.lock().await.shutdown_all().await;
    client_task.await.unwrap_or_else(|e| error!("Failed to join the Discord task: {}", e));
    db_thread.join().unwrap_or_else(|_| error!("Failed to join the database task"));

    info!("Shutdown complete");
    Ok(())
}

async fn daily_check(http: Arc<Http>, db: ExecutorConnection, config_time: NaiveTime, selfid: UserId, mut token: oneshot::Receiver<()>) {
    let time_of_day = Utc::now().time();
    if time_of_day < config_time {
        let dur = (config_time - time_of_day).to_std().unwrap();
        info!("Today's daily check time is in {} minutes, sleeping", dur.as_secs()/60);
        tokio::select! {
            _ = sleep(dur) => {},
            _ = (&mut token) => {
                info!("Daily check task has been stopped");
                return;
            }
        }
    } else {
        info!("Today's daily check time has already passed, executing immidiately")
    }
    loop {
        info!("Running daily check");
        if let Err(e) = daily_check_once(&http, &db, selfid.0).await {
            error!("Error while running daily check: {}", e);
        }
        let utc_now = Utc::now();
        let next_fire = (utc_now + chrono::Duration::days(1))
            .with_hour(config_time.hour()).unwrap()
            .with_minute(config_time.minute()).unwrap();
        let fire_in = (next_fire - utc_now).to_std().unwrap();
        info!("Daily check completed");
        tokio::select! {
            _ = sleep(fire_in) => {},
            _ = (&mut token) => {
                info!("Daily check task has been stopped");
                break;
            }
        }
    }
}

async fn daily_check_once(http: &Http, db: &ExecutorConnection, selfid: u64) -> anyhow::Result<()> {
    let mut ignored_users = Vec::new();
    let mut ignored_guilds = Vec::new();
    let anniversaries = db.get_anniversaries_today().await?;
    for anv in anniversaries {
        if !ignored_guilds.contains(&anv.guild) && !ignored_users.contains(&(anv.guild, anv.user)) {
            if !member_in_guild(http, anv.guild, anv.user).await? {
                ignored_users.push((anv.guild, anv.user));
                db.wipe_user(anv.user, anv.guild).await?;
                continue;
            }
            let midstr;
            let mid = if let Some(f) = anv.first_year {
                let ord = Utc::now().year()-(f as i32);
                midstr = format!("**{}**{} ", ord, Ordinal(ord).suffix());
                midstr.as_str()
            } else {
                ""
            };
            if let Err(e) = ChannelId::from(anv.channel).send_message(
                http,
                |m| m.content(format!("<@{}>, {}{:?} of {}", anv.user, mid, anv.a_type, anv.member)
            )).await {
                if let serenity::Error::Http(he) = &e {
                    if let serenity::http::HttpError::UnsuccessfulRequest(_) = **he {
                        ignored_guilds.push(anv.guild);
                        if !member_in_guild(http, anv.guild, selfid).await? {
                            db.unregister_guild(anv.guild).await?;
                        }
                        continue;
                    }
                }
                return Err(e.into())
            }
        }
    }
    ignored_users.clear();
    ignored_guilds.clear();
    Ok(())
}

async fn member_in_guild(http: &Http, guild: u64, user: u64) -> serenity::Result<bool> {
    if let Err(e) = http.get_member(guild, user).await {
        if let serenity::Error::Http(he) = &e {
            if let serenity::http::HttpError::UnsuccessfulRequest(ref resp) = **he {
                if resp.error.code == 10007 {
                    return Ok(false)
                }
            }
        }
        return Err(e);
    } else {
        Ok(true)
    }
}